# Changelog

## 0.1.2 - V12 update

Minor version bump to support the way V12 handles Handlebars templates slightly differently

## 0.1.1 - Minor bugfix

Fixed the missing click listener for opening the sheet for editing

## 0.1.0 - Initial release

Initial release of the module

### Features

* Ability to click on the Encounter name in the CombatTracker's header and set an alternate name for the encounter

## 