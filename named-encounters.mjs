// Register a small extra combat sheet that allows setting a name for the combat
class CombatNameSheet extends DocumentSheet {
    static get defaultOptions() {
        return foundry.utils.mergeObject(super.defaultOptions, {
            template: 'modules/named-encounters/combat_sheet.html',
            submitOnChange: true,
            submitOnClose: true
        });
    }
    
    async getData(options={}) {
        let context = super.getData(options)
        context.encounter_label = this.document.flags?.['named-encounters']?.label ?? game.i18n.localize('COMBAT.Encounter')
        return context
    }
}

// Update the name to show the tweaked combat name to users 
Hooks.on('renderCombatTracker', (app, [html], context) => {
    let el = html.querySelector('h4.encounter')
    // Don't add the click listener for non-GM users
    if (game.user.isGM) el.addEventListener('click', ev => new CombatNameSheet(app.viewed).render(true))
    // Don't edit the label if the flag isn't set
    if ((!app.viewed.flags?.['named-encounters']?.label)) return
    el.textContent = el.textContent.replace(
        game.i18n.localize('COMBAT.Encounter'),
        app.viewed.flags?.['named-encounters'].label
    )
})

Hooks.on('init', () => {
    // Register the handlebars template manually; it's a single line, not something that needs a whole file
    let compiled = Handlebars.compile('<form><label>{{localize "NAMED_ENCOUNTERS.encounter_label"}}<input name="flags.named-encounters.label" value="{{encounter_label}}"></label></form>')
    let path = 'modules/named-encounters/combat_sheet.html'
    Handlebars.registerPartial(path, compiled)
    if (foundry.utils.isNewerVersion(12, game.version)) _templateCache[path] = compiled
})

