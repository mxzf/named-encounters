# Named Encounters

This module is designed to add the option to name encounters with custom names for easier organization.

## Usage

Click the "Encounter" name in header of the combat tracker, a window will pop up where you can specify an alternate name. 

## Installation

Module manifest: https://gitlab.com/mxzf/named-encounters/-/releases/permalink/latest/downloads/module.json
